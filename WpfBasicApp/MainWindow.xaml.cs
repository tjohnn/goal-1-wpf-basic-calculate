﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WpfBasicApp
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Calculate(object sender, RoutedEventArgs e)
        {
            double operand1 = 0;
            double operand2 = 0;
            bool parsed1 = double.TryParse(operand_one.Text, out operand1);
            bool parsed2 = double.TryParse(operand_two.Text, out operand2);
            if(parsed1 && parsed2)
            {
                result_label.Content = "jnj";
                switch (my_operator.SelectedValue.ToString())
                {
                    case "+":
                        result_label.Content = (operand1 + operand2) + "";
                        break;
                    case "-":
                        result_label.Content = (operand1 - operand2) + "";
                        break;
                    case "*":
                        result_label.Content = (operand1 * operand2) + "";
                        break;
                    case "/":
                        result_label.Content = (operand1 / operand2) + "";
                        break;
                    case "%":
                        result_label.Content = (operand1 % operand2) + "";
                        break;
                }
            }
            
        }
    }
}
